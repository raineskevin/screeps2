var roleUpgrader = require('role.upgrader');
var roleRepairer2 = {

    /** @param {Creep} creep **/
    run: function(creep) {

	    if(creep.memory.repairing && creep.store[RESOURCE_ENERGY] == 0) {
            creep.memory.repairing = false;
            creep.say('🔄 harvest');
	    }
	    if(!creep.memory.repairing && creep.store.getFreeCapacity() == 0) {
	        creep.memory.repairing = true;
	        creep.say('🚧 repair');
	    }

	    if(creep.memory.repairing) {
	        var targets = creep.room.find(FIND_STRUCTURES, {
                filter: function(structure){
	                return (structure.hits < structure.hitsMax) && (structure.structureType != STRUCTURE_WALL) && (structure.structureType != STRUCTURE_RAMPART)
	            }
            })
            if(targets.length) {
                if (creep.repair(targets[0]) == ERR_NOT_IN_RANGE) {
                creep.moveTo(targets[0], { visualizePathStyle: { stroke: '#ffffff' } })
            creep.repair(targets[0])
                }
            }
            if(targets.length == 0) {
                var targetsWalls = creep.room.find(FIND_STRUCTURES, {
                filter: function(structure){
	                return (structure.hits < 250000) && (structure.structureType == STRUCTURE_WALL)
	            }
            })
            if(targetsWalls.length == 0) {
                roleUpgrader.run(creep)
            }
            if (creep.repair(targetsWalls[0]) == ERR_NOT_IN_RANGE) {
                creep.moveTo(targetsWalls[0], { visualizePathStyle: { stroke: '#ffffff' } })
            creep.repair(targetsWalls[0])                
                }
            }           
            
	    }
	    else {
	        var sources = creep.room.find(FIND_SOURCES);
            if(creep.harvest(sources[0]) == ERR_NOT_IN_RANGE) {
                creep.moveTo(sources[0], {visualizePathStyle: {stroke: '#ffaa00'}});
            }
	    }
	}
};

module.exports = roleRepairer2;