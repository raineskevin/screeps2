var manageRoles = {
  /** @param {Creep} creep **/
  run: function (creep) {
    const roles = [
      { name: "harvester", count: 3 },
      { name: "upgrader", count: 2 },
      { name: "builder", count: 1 },
      { name: "repairer2", count: 1 },
      { name: "harvester2", count: 1 },
    ];

    for (role of roles) {
      role.creeps = _.filter(
        Game.creeps,
        (creep) => creep.memory.role == role.name
      );
        
    //if (harvester.creeps.length == 0) {
    //    Game.spawns["Spawn1"].spawnCreep([WORK, CARRY, MOVE], harvester1, {memory: { role: role.harvester },
    //                                                                      });
    // }

    //  console.log(`${role.name}s: ${role.creeps.length}/${role.count}`);

      // console.log(`count: ${role.count} creeps: ${role.creeps.length}`);
      if (role.creeps.length < role.count) {
        var newName = `${role.name}${Game.time}`;
        console.log(`Spawning new ${role.name}: ${newName}`);
        Game.spawns["Spawn1"].spawnCreep([WORK, WORK, CARRY, CARRY, MOVE, MOVE], newName, {
          memory: { role: role.name },
        });
      }
    }
  },
};

module.exports = manageRoles;
