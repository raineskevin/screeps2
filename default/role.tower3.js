//const { cl, j } = require("logs");

var roleTower3 = {
  run: function () {
    Object.values(Game.rooms).forEach((room) => {
      Object.values(findTowers(room)).forEach((tower) => {
        // TODO JJ Make classes so tower.attack()
        attack(tower);
        repair(tower);
      });
    });
  },
};

function attack(tower) {
  const closestHostile = tower.pos.findClosestByRange(FIND_HOSTILE_CREEPS);
  if (closestHostile) {
    tower.attack(closestHostile);
  }
}

function findTowers(room) {
  return room.find(FIND_STRUCTURES, {
    filter: (structure) => {
      return structure.structureType === STRUCTURE_TOWER;
    },
  });
}

function repair(tower) {
	        var targets = tower.room.find(FIND_STRUCTURES, {
                filter: function(structure){
	                return (structure.hits < structure.hitsMax) && (structure.structureType != STRUCTURE_WALL) && (structure.structureType != STRUCTURE_RAMPART)
	            }
            })
            if(targets.length) {
                tower.repair(targets[0])
                }
            if(targets.length == 0) {
                var targetsWalls = tower.room.find(FIND_STRUCTURES, {
                filter: function(structure){
	                return (structure.hits < 250000) && (structure.structureType == STRUCTURE_WALL)
	            }
            })
            tower.repair(targetsWalls[0])                
                }
	    }
module.exports = roleTower3;