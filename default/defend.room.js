var defendRoom = {

run: function(tower) {
    var hostiles = Game.rooms['W2N2'].find(FIND_HOSTILE_CREEPS);
    if(hostiles) {
        var towers = Game.rooms['W2N2'].find(
            FIND_MY_STRUCTURES, {filter: {structureType: STRUCTURE_TOWER}});
        towers.forEach(tower => tower.attack(hostiles[0]));
    }
}
}
module.exports = defendRoom;