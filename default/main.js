var roleHarvester = require('role.harvester');
var roleHarvester2 = require('role.harvester2');
var roleBuilder = require('role.builder');
var roleUpgrader = require('role.upgrader');
var roleRepairer2 = require('role.repairer2');
var manageRoles = require('manage.roles');
var roleTower3 = require('role.tower3');
var defendRoom = require('defend.room');
var roleScout = require('role.scout');


module.exports.loop = function () 
{
        for(var name in Memory.creeps) {
        if(!Game.creeps[name]) {
            delete Memory.creeps[name];
            console.log('Clearing non-existing creep memory:', name);
        }
    }

{ roleTower3.run()};

    var harvesters = _.filter(Game.creeps, (creep) => creep.memory.role == 'harvester');
    
    if (harvesters.length < 1) 
        { Game.spawns['Spawn1'].spawnCreep([WORK, CARRY, MOVE], 'harvester1',
                { memory: { role: 'harvester' } } ) } ;


    manageRoles.run()
    
    for(var name in Game.creeps) {
        var creep = Game.creeps[name];
        if(creep.memory.role == 'harvester') {
            roleHarvester.run(creep);
        }
        if(creep.memory.role == 'harvester2') {
            roleHarvester2.run(creep);
        }
        if(creep.memory.role == 'builder') {
            roleBuilder.run(creep);
        }
        if(creep.memory.role == 'upgrader') {
            roleUpgrader.run(creep);
        }
        if(creep.memory.role == 'repairer2') {
            roleRepairer2.run(creep);
        }
    }
}