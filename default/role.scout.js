Game.map.describeExits("W7N7")[7]
var roleScout = {

    /** @param {Creep} creep **/
    run: function(creep) {
        if(creep.room.name == "W7N7"){
            //creep.moveTo(0,13);
            console.log('scount in W7N7')
            console.log(creep.memory.dirPref);
            if(creep.memory.dirPref < 0.5){
                creep.moveTo(25,0);
            }else if(creep.memory.dirPref < 1.5){
                creep.moveTo(0,25);
            }else if(creep.memory.dirPref < 2.5){
                creep.moveTo(49,25);
            }else{
                creep.moveTo(25,49);
            }
        }else{
            if(!creep.room.controller.my) {
                if(creep.claimController(creep.room.controller) == ERR_NOT_IN_RANGE) {
                    creep.moveTo(creep.room.controller);
                }
                else{
                    creep.claimController(creep.room.controller);
                }
            }else{
                creep.moveTo(25,25);
            }

        }
    }
};

module.exports = roleScout;