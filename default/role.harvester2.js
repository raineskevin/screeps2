var roleBuilder = require('role.builder');
var roleHarvester2 = {

    /** @param {Creep} creep **/
    run: function(creep) 
    {if(creep.store[RESOURCE_ENERGY] == 0) {
            creep.memory.harvest = true;
            creep.say('🔄 harvest');
	    }
	    if(creep.store.getFreeCapacity() == 0) {
	        creep.memory.harvest = false;
	        creep.say('🚧 drop');
	    }
        
        //Should change below part so that it first if there are spawn/extensions with free space, and else it checks for other structures.
        
     if(creep.memory.harvest == false) {
            var targets = creep.pos.findClosestByPath(FIND_STRUCTURES, {
                filter: (structure) => {
                    return (structure.structureType == STRUCTURE_SPAWN ||
                        structure.structureType == STRUCTURE_EXTENSION ||
                        structure.structureType == STRUCTURE_TOWER ||
                        structure.structureType == STRUCTURE_CONTAINER) &&
                        structure.store.getFreeCapacity(RESOURCE_ENERGY) > 0;
                }
            });
            if(targets !== null) {
                if(creep.transfer(targets, RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
                    creep.moveTo(targets, {visualizePathStyle: {stroke: '#ffffff'}});
                }
            }
            if(!targets) {roleBuilder.run(creep)};
        }
     if(creep.memory.harvest == true) {
	        var sources = creep.room.find(FIND_SOURCES);
            if(creep.harvest(sources[0]) == ERR_NOT_IN_RANGE) {
                creep.moveTo(sources[0], {visualizePathStyle: {stroke: '#ffaa00'}});
            }
	    }
    }
};



module.exports = roleHarvester2;
